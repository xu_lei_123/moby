%global _gitcommit_engine f417435 
%global _gitcommit_cli 4debf41
%global _source_engine moby-%{version}
%global _source_client cli-%{version}
%global _source_docker_init tini-0.19.0
%define _debugsource_template %{nil}

Name: 	  docker
Version:  25.0.3
Release:  13
Summary:  The open-source application container engine
License:  ASL 2.0
URL:	  https://www.docker.com
# https://github.com/docker/cli/archive/refs/tags/v25.0.3.tar.gz
Source0:  cli-%{version}.tar.gz
# https://github.com/moby/moby/archive/refs/tags/v25.0.3.tar.gz
Source1:  moby-%{version}.tar.gz
# https://github.com/krallin/tini/archive/refs/tags/v0.19.0.tar.gz
Source2:  tini-0.19.0.tar.gz
Source3:  docker.service
Source4:  docker.socket
Source5:  docker.sysconfig
Patch0000:  0001-fix-cve-2024-29018.patch
Patch0001:  0002-fix-cve-2024-32473.patch
Patch0002:  0003-add-loongarch64-seccomp-support.patch
Patch0003:  0004-fix-docker-swarm-run-failed-for-loongarch64.patch
Patch0004:  0005-cmd-dockerd-fix-non-constant-format-string-in-call-g.patch

Patch9000:  backport-CVE-2024-41110.patch
Patch9001:  backport-tini.c-a-function-declaration-without-a-prototype-is.patch

Requires: %{name}-engine = %{version}-%{release}
Requires: %{name}-client = %{version}-%{release}

# conflicting packages
Conflicts: docker-ce
Conflicts: docker-io
Conflicts: docker-engine-cs
Conflicts: docker-ee

%description
Docker is a product for you to build, ship and run any application as a
lightweight container.

%package engine
Summary: Docker daemon binary and related utilities

Requires: /usr/sbin/groupadd
Requires: runc
Requires: container-selinux >= 2:2.74
Requires: libseccomp >= 2.3
Requires: systemd
Requires: iptables
Requires: libcgroup
Requires: containerd
Requires: tar
Requires: xz

BuildRequires: bash
BuildRequires: ca-certificates
BuildRequires: cmake
BuildRequires: device-mapper-devel
BuildRequires: gcc
BuildRequires: git
BuildRequires: glibc-static
BuildRequires: libarchive
BuildRequires: libseccomp-devel
BuildRequires: libselinux-devel
BuildRequires: libtool
BuildRequires: libtool-ltdl-devel
BuildRequires: make
BuildRequires: pkgconfig
BuildRequires: pkgconfig(systemd)
BuildRequires: selinux-policy-devel
BuildRequires: systemd-devel
BuildRequires: tar
BuildRequires: which
BuildRequires: golang  >= 1.18.0

%description engine
Docker daemon binary and related utilities

%package client
Summary: Docker client binary and related utilities

Requires: /bin/sh
BuildRequires: libtool-ltdl-devel

%description client
Docker client binary and related utilities

%prep
%setup -q -n %{_source_client}
%setup -q -T -n %{_source_engine} -b 1
%patch 0000 -p1
%patch 0001 -p1
%patch 0002 -p1
%patch 0003 -p1
%patch 9000 -p1
%setup -q -T -n %{_source_docker_init} -b 2
%patch 9001 -p1

%build
export GO111MODULE=off
# build docker daemon
export DOCKER_GITCOMMIT=%{_gitcommit_engine}
export DOCKER_BUILDTAGS="exclude_graphdriver_btrfs"

pushd %{_builddir}/%{_source_engine}
AUTO_GOPATH=1 VERSION=%{version} PRODUCT=docker hack/make.sh dynbinary
popd

# build docker-tini
pushd %{_builddir}/%{_source_docker_init}
cmake .
make tini-static
popd

# build cli
pushd %{_builddir}/%{_source_client}
mkdir -p .gopath/src/github.com/docker/cli
export GOPATH=`pwd`/.gopath
rm -rf .gopath/src/github.com/docker/cli
ln -s %{_builddir}/%{_source_client} .gopath/src/github.com/docker/cli
pushd .gopath/src/github.com/docker/cli
DISABLE_WARN_OUTSIDE_CONTAINER=1 make VERSION=%{version} GITCOMMIT=%{_gitcommit_cli} dynbinary
popd
popd

%check
# check for daemon
ver="$(%{_builddir}/%{_source_engine}/bundles/dynbinary-daemon/dockerd --version)"; \
    test "$ver" = "Docker version %{version}, build %{_gitcommit_engine}" && echo "PASS: daemon version OK" || (echo "FAIL: daemon version ($ver) did not match" && exit 1)
# check for client
ver="$(%{_builddir}/%{_source_client}/build/docker --version)"; \
    test "$ver" = "Docker version %{version}, build %{_gitcommit_cli}" && echo "PASS: cli version OK" || (echo "FAIL: cli version ($ver) did not match" && exit 1)


%install
# install daemon binary
install -D -p -m 0755 $(readlink -f %{_builddir}/%{_source_engine}/bundles/dynbinary-daemon/dockerd) %{buildroot}%{_bindir}/dockerd

# install proxy
install -D -p -m 0755 %{_builddir}/%{_source_engine}/bundles/dynbinary-daemon/docker-proxy %{buildroot}%{_bindir}/docker-proxy

# install tini
install -D -p -m 755 %{_builddir}/%{_source_docker_init}/tini-static %{buildroot}%{_bindir}/docker-init

# install systemd scripts
install -D -m 0644 %{SOURCE3} %{buildroot}%{_unitdir}/docker.service
install -D -m 0644 %{SOURCE4} %{buildroot}%{_unitdir}/docker.socket

# for additional args
install -Dpm 644 %{SOURCE5} %{buildroot}%{_sysconfdir}/sysconfig/docker

# install docker client
install -p -m 0755 $(readlink -f %{_builddir}/%{_source_client}/build/docker) %{buildroot}%{_bindir}/docker

# add bash, zsh, and fish completions
install -d %{buildroot}%{_datadir}/bash-completion/completions
install -d %{buildroot}%{_datadir}/zsh/vendor-completions
install -d %{buildroot}%{_datadir}/fish/vendor_completions.d
install -p -m 644 %{_builddir}/%{_source_client}/contrib/completion/bash/docker %{buildroot}%{_datadir}/bash-completion/completions/docker
install -p -m 644 %{_builddir}/%{_source_client}/contrib/completion/zsh/_docker %{buildroot}%{_datadir}/zsh/vendor-completions/_docker
install -p -m 644 %{_builddir}/%{_source_client}/contrib/completion/fish/docker.fish %{buildroot}%{_datadir}/fish/vendor_completions.d/docker.fish

# add docs
install -d %{buildroot}%{_pkgdocdir}
install -p -m 644 %{_builddir}/%{_source_client}/{LICENSE,MAINTAINERS,NOTICE,README.md} %{buildroot}%{_pkgdocdir}

%files
# empty as it depends on engine and client

%files engine
%config(noreplace) %{_sysconfdir}/sysconfig/docker
%{_bindir}/dockerd
%{_bindir}/docker-proxy
%{_bindir}/docker-init
%{_unitdir}/docker.service
%{_unitdir}/docker.socket

%files client
%{_bindir}/docker
%{_datadir}/bash-completion/completions/docker
%{_datadir}/zsh/vendor-completions/_docker
%{_datadir}/fish/vendor_completions.d/docker.fish
%doc %{_pkgdocdir}

%post
%systemd_post docker.service
if ! getent group docker > /dev/null; then
    groupadd --system docker
fi

%preun
%systemd_preun docker.service docker.socket

%postun
%systemd_postun_with_restart docker.service

%changelog
* Tue Oct 29 2024 xulei <xulei@xfusion.com> - 25.0.3-13
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:cmd-dockerd-fix-non-constant-format-string-in-call-g.patch

* Tue Oct 29 2024 xuguangmin <xuguangmin@kylinos.cn> - 25.0.3-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Remove RPM Build Warning Caused by .spec File

* Mon Sep 9 2024 tiberium <jinzhe.oerv@isrc.iscas.ac.cn> - 25.0.3-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport upstream patch to solve -Wstrict-prototypes error

* Fri Jul 26 2024 zhangxianting <zhangxianting@uniontechc.om> - 25.0.3-10
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2024-41110

* Fri Jul 12 2024 lvxiangcong <lvxiangcong@kylinos.cn> - 25.0.3-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix docker swarm run failed for loongarch64

* Tue Jul 02 2024 zhangbowei<zhangbowei@kylinos.cn> - 25.0.3-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix bug by using docker-proxy in the source file to get better compatibility

* Fri Jun 28 2024 shechenglong<shechenglong@xfusion.com> - 25.0.3-7
- DESC:software package name moby is changed to docker

* Fri Jun 14 2024 zhaixiaojuan<zhaixiaojuan@loongson.cn> - 25.0.3-6
- DESC:add loongarch64 seccomp support

* Tue May 14 2024 wanglimin<wanglimin@xfusion.com> - 25.0.3-5
- DESC:clean dependency between engine and cli

* Sat May 11 2024 lvxiangcong<lvxiangcong@kylinos.cn> - 25.0.3-4
- DESC:fix install error failed to docker.service does not exit

* Mon Apr 22 2024 lvxiangcong<lvxiangcong@kylinos.cn> - 25.0.3-3
- DESC:fix cve-2024-32473

* Wed Apr 17 2024 lvxiangcong<lvxiangcong@kylinos.cn> - 25.0.3-2
- DESC:fix cve-2024-29018

* Tue Feb 06 2024 shechenglong<shechenglong@xfusion.com> - 25.0.3-1
- DESC:update to 25.0.3

* Thu Dec 28 2023 maokecheng<maokecheng@xfusion.com> - 20.10.24-7
- Adapt docker-runc to be changed to runc

* Tue Sep 26 2023 xulei<xulei@xfusion.com> - 20.10.24-6
- Fix the missing socketGroup permissions for only installing moby-engine

* Mon Sep 18 2023 xulei<xulei@xfusion.com> - 20.10.24-5
- Optimize the apply patch method

* Thu Sep 14 2023 xulei<xulei@xfusion.com> - 20.10.24-4
- DESC: Fix missing runc dependencies
        The declaration conflicts with the installation of docker-engine

* Mon Sep 4 2023 xulei<xulei@xfusion.com> - 20.10.24-3
- Fix the conflict libnetwork installation

* Sun Jul 16 2023 xulei<xulei@xfusion.com> - 20.10.24-2
- DESC: fix non-blocking awslogs log drop bug
        fix panic if mount is not a volume

* Mon Apr 10 2023 xulei<xulei@xfusion.com> - 20.10.24-1
- DESC: update to 20.10.24

* Tue Apr 4 2023 zhangzhihui<zhangzhihui@xfusion.com> - 20.10.23-3
- DESC: sync upstream patch
        Dockerfile: configure code dir as "safe" directory

* Fri Mar 31 2023 zhangzhihui<zhangzhihui@xfusion.com> - 20.10.23-2
- DESC: sync upstream patch to update containerd to v1.6.16

* Wed Mar 29 2023 xulei<xulei@xfusion.com> - 20.10.23-1
- DESC:update to 20.10.23

* Wed Mar 8 2023 xulei<xulei@xfusion.com> - 20.10.21-4
- DESC: enhance container behavior
        1.stop docker.socket before uninstall docker.
        2.container keep running when restart docker service.
        3.when containerd exits abnormally, it can be automatically pulled up.
        4.add some dockerd options.
        5.change to BuildRequires golang-1.18.0

* Wed Dec 28 2022 xulei<xulei@xfusion.com> - 20.10.21-3
- DESC: change to BuildRequires golang-1.17.3

* Wed Dec 21 2022 wanglimin<wanglimin@xfusion.com> - 20.10.21-2
- DESC: revert any to interface{} temporarily to allow builtable with golang-1.17.x
-       it will be withdrawed if golang upgrade to 1.18.x in the branch

* Wed Dec 14 2022 wanglimin<wanglimin@xfusion.com> - 20.10.21-1
- DESC: initial docker-20.10.21-1
